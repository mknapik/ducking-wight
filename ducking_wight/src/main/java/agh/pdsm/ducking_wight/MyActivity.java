package agh.pdsm.ducking_wight;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.esri.android.map.GraphicsLayer;
import com.esri.android.map.LocationService;
import com.esri.android.map.MapView;
import com.esri.android.map.ags.ArcGISTiledMapServiceLayer;
import com.esri.core.geometry.GeometryEngine;
import com.esri.core.geometry.Point;
import com.esri.core.geometry.Polyline;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.map.Graphic;
import com.esri.core.symbol.SimpleFillSymbol;
import com.esri.core.symbol.SimpleLineSymbol;
import com.esri.core.symbol.SimpleMarkerSymbol;

import org.apache.http.conn.HttpHostConnectException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import agh.pdsm.ducking_wight.server.CommandCenterService;
import agh.pdsm.ducking_wight.server.json.Coordinates;
import agh.pdsm.ducking_wight.server.json.Polygon;
import agh.pdsm.ducking_wight.server.json.Trip;
import retrofit.RestAdapter;

public class MyActivity extends Activity {
    // Spatial references used for projecting points
    final static SpatialReference wm = SpatialReference.create(102100);
    final static SpatialReference egs = SpatialReference.create(4326);
    private final static int[] COLORS = new int[]{
            Color.RED, Color.BLACK, Color.DKGRAY, Color.GRAY, Color.LTGRAY, Color.WHITE, Color.RED,
            Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.MAGENTA, Color.TRANSPARENT};
    private static int color = 0;
    final SimpleLineSymbol polygonBorder = new SimpleLineSymbol(Color.BLUE, 5);
    final SimpleLineSymbol tripLineStyle = new SimpleLineSymbol(Color.YELLOW, 3);
    final List<ArcGISTiledMapServiceLayer> maps = new ArrayList<ArcGISTiledMapServiceLayer>(7);
    final GraphicsLayer currentPositionLayer = new GraphicsLayer();
    final String address = "http://rescue-command-center.herokuapp.com";
    // Example usage of API
    final RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint(address)
            .build();
    final CommandCenterService api = restAdapter.create(CommandCenterService.class);
    final Map<Long, Map<Long, Trip>> userTrips = new HashMap<Long, Map<Long, Trip>>();
    private final Logger log = Logger.getLogger(this.getClass().toString());
    private final GraphicsLayer graphicsLayer = new GraphicsLayer();
    MapView mapView;
    ArcGISTiledMapServiceLayer basemapTopo;
    ArcGISTiledMapServiceLayer basemapImagery;
    ArcGISTiledMapServiceLayer basemapStreet;
    Point locationPoint = null;
    private final List<Polygon> teamPolygons = new ArrayList<Polygon>();
    private boolean newData = false;
    private boolean serverConnectionProblem = false;
    private Trip currentTrip = null;
    private final Boolean lockCurrentTrip = true;
    private final Boolean lockTeamPolygons = true;
    private final Long userId = 1L;
    private final Long teamId = 1L;

    private static int getTeamColor(final Long teamId) {
        if (teamId == 1) {
            return Color.RED;
        } else if (teamId == 2) {
            return Color.GREEN;
        } else if (teamId == 3) {
            return Color.BLUE;
        } else if (teamId == 4) {
            return Color.GRAY;
        } else {
            return COLORS[(color++) % COLORS.length];
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Retrieve the map and initial extent from XML layout
        mapView = (MapView) findViewById(R.id.map);
        /* create an initial basemap */
       /* create an initial basemap */

        basemapTopo = new ArcGISTiledMapServiceLayer(this.getResources().getString(R.string.WORLD_TOPO_MAP));
        basemapImagery = new ArcGISTiledMapServiceLayer(this.getResources().getString(R.string.WORLD_IMAGERY));
        basemapStreet = new ArcGISTiledMapServiceLayer(this.getResources().getString(R.string.WORLD_STREET_MAP));

        // Add basemap to MapView
        mapView.addLayer(basemapImagery);
        mapView.addLayer(basemapTopo);
        mapView.addLayer(basemapStreet);
        mapView.addLayer(currentPositionLayer);
        // set visibility
        basemapTopo.setVisible(true);
        basemapStreet.setVisible(false);
        basemapImagery.setVisible(false);
        currentPositionLayer.setVisible(true);

        final LocationService ls = mapView.getLocationService();
        ls.setLocationListener(new MyLocationListener());
        ls.start();

        ls.setAutoPan(false);

        mapView.addLayer(graphicsLayer);

        callAsynchronousDrawTask();
//        callAsynchronousSendTripTask();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.unpause();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basemap_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.World_Imagery:
                basemapTopo.setVisible(false);
                basemapImagery.setVisible(true);
                basemapStreet.setVisible(false);
                return true;
            case R.id.World_Topo:
                basemapTopo.setVisible(true);
                basemapImagery.setVisible(false);
                basemapStreet.setVisible(false);
                return true;
            case R.id.World_Street:
                basemapTopo.setVisible(false);
                basemapImagery.setVisible(false);
                basemapStreet.setVisible(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Point pointProject(final Point point) {
        return (Point) GeometryEngine.project(point, egs, wm);
    }

    private void callAsynchronousDrawTask() {
        final Handler handler = new Handler();
        final Timer timer = new Timer();
        final TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        synchronized (lockTeamPolygons) {
                            try {
                                if (newData) {
                                    log.log(Level.INFO, "has new data, drawing...");
                                    graphicsLayer.removeAll();

                                    for (final Polygon polygon : teamPolygons) {
                                        final Map<Long, Trip> teamTrips = userTrips.get(polygon.getTeamId());

                                        drawPolygon(polygon, graphicsLayer);

                                        if (teamTrips != null) {
                                            for (final Long userId : teamTrips.keySet()) {
                                                final Trip trip = teamTrips.get(userId);
                                                drawTrip(trip, graphicsLayer);
                                            }
                                        }
                                    }
                                }
//                            final BackgroundDrawTask performBackgroundTask = new BackgroundDrawTask();
//                            // PerformBackgroundTask this class is the class that extends AsynchTask
//                            performBackgroundTask.execute();
                            } catch (final Exception e) {
                                log.log(Level.SEVERE, e.toString());
                                // TODO Auto-generated catch block
                            } finally {
                                newData = false;
                            }
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 1000, 10000);
    }

    private void drawPolygon(final Polygon polygon, final GraphicsLayer graphicsLayer) {
        final SimpleFillSymbol fill = new SimpleFillSymbol(getTeamColor(polygon.getTeamId()));
        fill.setAlpha(100);

        com.esri.core.geometry.Polygon poly = null;
        log.log(Level.INFO, "polygon " + polygon.toString());
        for (final Coordinates coordinates : polygon.getVertex()) {
            final Point point = new Point(coordinates.getLongitude(), coordinates.getLatitude());

            log.log(Level.FINE, "\t" + Double.toString(coordinates.getLatitude()) + ", " + Double.toString(coordinates.getLongitude()));
            log.log(Level.FINE, "\t" + Double.toString(point.getY()) + ", " + Double.toString(point.getX()));
            if (poly == null) {
                poly = new com.esri.core.geometry.Polygon();
                poly.startPath(point);
            } else {
                poly.lineTo(point);
            }
        }
        if (poly != null) {
            // Create a Graphic and add polyline geometry
            graphicsLayer.addGraphic(new Graphic(poly, polygonBorder));
            graphicsLayer.addGraphic(new Graphic(poly, fill));
        }
    }

    private void drawTrip(final Trip trip, final GraphicsLayer graphicsLayer) {
        Polyline poly = null;
        log.log(Level.INFO, "trip " + trip.toString());
        for (final Coordinates coordinates : trip.getVertex()) {
            final Point point = new Point(coordinates.getLongitude(), coordinates.getLatitude());


            log.log(Level.FINE, "\t" + Double.toString(coordinates.getLatitude()) + ", " + Double.toString(coordinates.getLongitude()));
            log.log(Level.FINE, "\t" + Double.toString(point.getY()) + ", " + Double.toString(point.getX()));
            if (poly == null) {
                poly = new Polyline();
                poly.startPath(point);
            } else {
                poly.lineTo(point);
            }
        }
        if (poly != null) {
            // Create a Graphic and add polyline geometry
            graphicsLayer.addGraphic(new Graphic(poly, tripLineStyle));
        }
    }

    private void showConnectionProblemAlert(final HttpHostConnectException e) {
        final MyActivity myActivity = this;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(myActivity)
                        .setTitle("Server connection problem")
                        .setMessage("Check your internet connection and if the server is running!")
                        .setNeutralButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int which) {
                                // continue with delete
                                dialog.cancel();
                            }
                        })
                        .setIcon(R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    private class MyLocationListener implements LocationListener {

        public MyLocationListener() {
            super();
        }

        /**
         * If location changes, update our current location. If being found for
         * the first time, zoom to our current position with a resolution of 20
         */
        @Override
        public void onLocationChanged(final Location loc) {
            if (loc == null) {
                return;
            }
            final boolean zoomToMe = (locationPoint == null);
            final double longitude = loc.getLongitude();
            final double latitude = loc.getLatitude();
            locationPoint = pointProject(new Point(longitude, latitude));
            if (zoomToMe) {
                mapView.zoomToResolution(locationPoint, 1.0);
            }
            log.log(Level.INFO, "location: latitude, longitude: " + Double.toString(latitude) + ", " + Double.toString(longitude));
            log.log(Level.INFO, "projected: latitude, longitude: " + Double.toString(locationPoint.getY()) + ", " + Double.toString(locationPoint.getX()));

            // create a simple marker symbol to be used by our graphic
            final SimpleMarkerSymbol sms = new SimpleMarkerSymbol(Color.RED, 10, SimpleMarkerSymbol.STYLE.DIAMOND);
            // create the graphic using the symbol and point geometry
            final Graphic graphic = new Graphic(locationPoint, sms);
            currentPositionLayer.removeAll();
            currentPositionLayer.addGraphic(graphic);

            synchronized (lockCurrentTrip) {
                if (currentTrip == null) {
                    currentTrip = new Trip(userId, teamId, new ArrayList<Coordinates>());
                }
                log.log(Level.INFO, "Add coordinate to the currentTrip");
                currentTrip.getVertex().add(new Coordinates(locationPoint.getX(), locationPoint.getY()));
                log.log(Level.INFO, currentTrip.toString());
            }

            final Runnable r = getPolygons();
            new Thread(r).start();
        }

        private Runnable getPolygons() {
            return new Runnable() {
                public void run() {
                    log.log(Level.INFO, "Running getAllPolygons");
                    try {
                        final List<Polygon> polygons = api.getAllPolygons().getPolygons();
                        log.log(Level.INFO, polygons.toString());

                        synchronized (lockTeamPolygons) {
                            teamPolygons.clear();
                            for (final Polygon polygon : polygons) {
                                log.log(Level.INFO, polygon.toString());
                                teamPolygons.add(polygon);
                            }

                            final List<Trip> trips = api.getTrips().getTrips();
                            log.log(Level.INFO, trips.toString());

                            for (final Trip trip : trips) {
                                if (!userTrips.containsKey(trip.getTeamId())) {
                                    userTrips.put(trip.getTeamId(), new HashMap<Long, Trip>());
                                }
                                final Map<Long, Trip> teamTrips = userTrips.get(trip.getTeamId());
                                teamTrips.put(trip.getUserId(), trip);
                            }
                            newData = true;
                            serverConnectionProblem = false;
                        }
                    } catch (final retrofit.RetrofitError error) {
                        error.printStackTrace();
                        log.log(Level.WARNING, error.getUrl());
                        try {
                            throw error.getCause();
                        } catch (final HttpHostConnectException e) {
                            if (!serverConnectionProblem) {
                                showConnectionProblemAlert(e);
                            }
                            serverConnectionProblem = true;
                        } catch (final Throwable ignore) {
                        }
                    }

                    log.log(Level.INFO, "Sending a trip sendTrip");
                    synchronized (lockCurrentTrip) {
                        if (currentTrip == null) {
                            log.log(Level.INFO, "No new position to send (null)");
                        } else if (currentTrip.getVertex().isEmpty()) {
                            log.log(Level.INFO, "No new position to send (empty)");
                        } else {
                            try {
                                final String result = api.sendTrips(currentTrip);

                                currentTrip = null;
                            } catch (final retrofit.RetrofitError error) {
                                error.printStackTrace();
                                log.log(Level.WARNING, error.getUrl());
                                try {
                                    throw error.getCause();
                                } catch (final HttpHostConnectException e) {
                                    if (!serverConnectionProblem) {
                                        showConnectionProblemAlert(e);
                                    }
                                    serverConnectionProblem = true;
                                } catch (final Throwable ignore) {
                                }
                            }
                        }
                    }
                }
            };
        }

        @Override
        public void onProviderDisabled(final String provider) {
            Toast.makeText(getApplicationContext(), "GPS Disabled", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(final String provider) {
            Toast.makeText(getApplicationContext(), "GPS Enabled", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(final String provider, final int status, final Bundle extras) {
            log.log(Level.ALL, "statusChanged: " + provider);
        }

    }

    class BackgroundDrawTask extends AsyncTask {

        @Override
        protected Object doInBackground(final Object[] objects) {
            return null;
        }
    }
}
