package agh.pdsm.ducking_wight.server;

import agh.pdsm.ducking_wight.server.json.Polygon;
import agh.pdsm.ducking_wight.server.json.Polygons;
import agh.pdsm.ducking_wight.server.json.Trip;
import agh.pdsm.ducking_wight.server.json.Trips;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;


public interface CommandCenterService {
    @GET("/")
    void index();

    @GET("/server/get/polygons")
    Polygons getAllPolygons();

    @GET("/server/get/polygons/{id}")
    Polygons getPolygonsByTeam(@Path("id") Long id);

    @GET("/server/get/trips")
    Trips getTrips();

    @POST("/server/send/polygons")
    void sendPolygons(@Body Polygon request);

    @POST("/server/send/trips")
    String sendTrips(@Body Trip trip);

}