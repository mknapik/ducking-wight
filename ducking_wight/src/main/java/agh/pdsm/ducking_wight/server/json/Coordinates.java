package agh.pdsm.ducking_wight.server.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class Coordinates {
	private Float longitude;
	private Float latitude;

    public Coordinates(final Double longitude, final Double latitude) {
        this.longitude = longitude.floatValue();
        this.latitude = latitude.floatValue();
    }
}
