package agh.pdsm.ducking_wight.server.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class Polygons {
	private List<Polygon> polygons;
}
