package agh.pdsm.ducking_wight.server.json;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
public class Trip {
	private Long userId;
	private Long teamId;

	private List<Coordinates> vertex;
}
