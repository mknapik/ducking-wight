#!/usr/bin/env python2
import web
import json


def notfound():
    return web.notfound(json.dumps({'ok':0, 'errcode': 404}))

def internalerror():
    return web.internalerror(json.dumps({'ok':0, 'errcode': 500}))


urls = (
    '/server/send/polygons', 'handleRequest',
    '/server/get/polygons', 'handleRequest',
    '/server/send/point', 'pathRequest',
    '/server/get/trips', 'pathRequest'
)


app = web.application(urls, globals())
app.notfound = notfound
app.internalerror = internalerror


polyList = []
#teams = {0:[0,1,2,3], 1:[4,5,6,7], 2:[8,9,10,11], 3:[12,13,14,15]}
teams = [0,0,0,0, 1,1,1,1, 2,2,2,2, 3,3,3,3]
paths = {}
for i in range(16): paths[unicode(i)] = []



class handleRequest:

    def GET(self):
        web.header('Content-Type', 'application/json')
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        result = json.dumps({'polygons': polyList})

        i = web.input()
        if 'callback' in i:
            return '{}({});'.format(i['callback'], result)
        else:
            return result

    def OPTIONS(self):
        web.header('Content-Type', 'application/json')
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')
        return {'key' : 'value'}

    def POST(self):
        try:
            #i = web.input()
            #print(i)
            
            j = json.loads(web.data()) # you can get data use this method
            polyList.append(j)
            print(polyList)
        except Exception as e:
            print(e)

class pathRequest:

    def GET(self):
        result = []
        for user, path in paths.iteritems():
            result.append({'userId': user, 'teamId': teams[int(user)], 'vertex': path})
        print(result)
        result = json.dumps({'trips': result})

        i = web.input()
        if 'callback' in i:
            return '{}({});'.format(i['callback'], result)
        else:
            return result


    def OPTIONS(self):
        web.header('Content-Type', 'application/json')
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')
        return {'key' : 'value'}

    def POST(self):
        try:
            j = json.loads(web.data()) 
            #print('userId: {}'.format(type(j['userId'])))
            #print('vertex: {}'.format(j['vertex']))
            #print('paths: {}'.format(paths))

            paths[j['userId']].extend(j['vertex'])
            print('paths: {}'.format(paths))
        except Exception as e:
            print('Exc?')
            print(e)

if __name__ == "__main__":
    app.run()
